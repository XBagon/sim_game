extern crate specs;
#[macro_use]
extern crate specs_derive;
#[macro_use]
extern crate serde_derive;
extern crate bincode;
extern crate byteorder;
extern crate serde;

use specs::prelude::*;
use std::{thread,time};
use std::net::{TcpListener,TcpStream};
use std::io::{Read as io_read,Write as io_write};
use bincode::{serialize, deserialize};
use std::sync::{Arc,RwLock};
use byteorder::{ReadBytesExt,WriteBytesExt,LittleEndian};
use serde::Serialize;

fn main()
{
    let world = Arc::new(RwLock::new(World::new()));
    let world_clone = world.clone();
    {
        let mut world = world.write().unwrap();
        world.register::<Position>();
        world.register::<Deposit>();
        let _ent = world.create_entity().with(Position { x: 4.0, y: 7.0 }).build();
        let _deposit = world.create_entity().with(Deposit { material : 0, richness : 1.0, amount : 10e6 } ).build();
    }

    let mut dispatcher = DispatcherBuilder::new()
        .with(TickSystem, "tick_system", &[])
        .build();

    thread::spawn(move|| {
        for inc in TcpListener::bind("127.0.0.1:1234").unwrap().incoming(){
            let world_clone = world_clone.clone();
            thread::spawn(move||{
                let mut stream = inc.unwrap();
                loop {
                    let size = stream.read_u32::<LittleEndian>().unwrap();
                    //let mut buffer = Vec::<u8>::with_capacity(size as usize);
                    let mut buffer = vec![0u8; size as usize];
                    let _package = stream.read_exact(&mut buffer[..]).unwrap();
                    let request : NetRequest = deserialize(&buffer).unwrap();
                    let world_clone = world_clone.read().unwrap();
                    match request {
                        NetRequest::Position(i) => send_net_package(&mut stream,
                                                                    &NetResultSend::Position(
                                                                        world_clone.read_storage::<Position>().join().get((&world_clone).entities().entity(i),&world_clone.entities()).unwrap()
                                                                    )
                        ),
                        NetRequest::Deposit(i) => send_net_package(&mut stream,
                                                                    &NetResultSend::Deposit(
                                                                        world_clone.read_storage::<Deposit>().join().get((&world_clone).entities().entity(i),&world_clone.entities()).unwrap()
                                                                    )
                        )
                    };
                }
            });
        }
    });

    loop {
        let time = time::Instant::now();
        dispatcher.dispatch(&mut world.write().unwrap().res);
        thread::sleep(time::Duration::from_millis(100) - time.elapsed());
    }
}

#[derive(Component, Serialize, Deserialize, Debug)]
struct Position {
    x: f64,
    y: f64
}

#[derive(Component, Serialize, Deserialize, Debug)]
struct Deposit {
    material : u64,
    richness : f64,
    amount : f64
}

struct TickSystem;

impl<'a> System<'a> for TickSystem {
    type SystemData = (WriteStorage<'a, Position>);

    fn run(&mut self, data: Self::SystemData) {
        let mut pos = data;
        for pos in (&mut pos).join() {
            pos.x += 0.1;
            pos.y -= 0.1;
            println!("{:?}", pos);
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
enum NetRequest{
    Position(u32),
    Deposit(u32)
}

#[derive(Serialize, Debug)]
enum NetResultSend<'a>{
    Position(&'a Position),
    Deposit(&'a Deposit)
}

#[derive(Deserialize, Debug)]
enum NetResultReceive{
    Position(Position),
    Deposit(Deposit)
}

fn send_net_package<T>(connection :&mut TcpStream, request : T) where T: Serialize{
    let content = &serialize(&request).unwrap()[..];
    connection.write_u32::<LittleEndian>(content.len() as u32).unwrap();
    connection.write_all(content).unwrap();
}